##########################################################################################
##
## RUN FG ON SCHULER DATA EXP1
##

import os
import random

rootDir = os.path.abspath('..')
outDir = rootDir+'/test/outputs/'

data_lists = ['3r6e-x1','5r4e-x1']#'4r5e'

# a = 0.5 and b = 100 are the ones used in the book
# 0 <= a <0
# b > -a

# a = 0 
# b = 1

#found a = 0.6 b = 2 to be optimal

# number of participants
N = 100
a_list = [0.6]
b_list = [2]

print('\nRunning bestpcfg on Schuler dataset...')
N=N//2 # participants per group
for a in a_list:
	for b in b_list:
		if not (b > -a): continue
		print('Parameters a=%g, b=%g' % (a,b))
		# For each participant
		for n in range(N):
			print('Participant: '+str(n+1))
			#Run each data file
			for data in data_lists:
				ID = str(n)+'_'+str(a)+'_'+str(b)+'_'+data
				# random_seed = n
				random_seed = random.randint(0,N)
				command = rootDir+'/bin/bestpcfg -corpus-counts '+rootDir+'/data/'+data+'.counts.txt -corpus-sentences '+rootDir+'/data/'+data+'.forms.txt -pya '+str(a)+' -pyb '+str(b)+' -sticky-concen 1 -sticky-dist 0.5 -pi 1 -sweeps 1000 -debug-file '+outDir+'/'+ID+'.FG-output-debug-0.txt -output-grammar '+outDir+'/'+ID+'.FG-output-0 -seed '+str(random_seed)+' -start-symbol START'
				os.system(command)

		# Run evaluation on output files
		os.system('python evaluate_outputs_exp1.py '+str(a)+' '+str(b)+' '+str(N))
		# os.system('nohup python3 evaluate_outputs_exp1.py '+str(a)+' '+str(b)+' '+str(N)+' &')
print('Grammars created!')













