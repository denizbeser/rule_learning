# Create Tolerance Principle test datasets, in FG format

rootDir = '/Users/denizbeser/Desktop/Stuff/Penn/Junior_Summer/Rule_Learning/fg-source-code'

data_lists = ['3r6e-x2-LangA','5r4e-x2-LangA','3r6e-x2-LangB','5r4e-x2-LangB']

freq_count_list = [24,12,8,6,6,4,4,4,4]
plural_count_list = [16,8,5,4,4,3,3,3,3]
data2index = {'3r6e-x2-LangA':[1,0,1,0,1,0,0,0,0],'5r4e-x2-LangA':[1,0,1,0,1,0,1,0,1],'3r6e-x2-LangB':[0,1,0,1,0,1,0,0,0],'5r4e-x2-LangB':[0,1,0,1,0,1,0,1,1]}

for data in data_lists:
	count_file = rootDir+'/data/'+data+'.counts.txt'
	form_file = rootDir+'/data/'+data+'.forms.txt'
	with open (count_file, 'w') as cf:
		with open (form_file, 'w') as ff:
			indices = data2index[data]
			# For each noun, print n_p plurals and remaining as singles
			for i in range(9):
				n_freq = freq_count_list[i]
				n_plural = plural_count_list[i]
				# Pick inflection
				inflection = 'ka' if indices[i] else 'e_'+str(i)
				# Plural
				cf.write(str(n_plural) + '\n')
				ff.write('((PLURAL (SINGULAR w'+str(i)+') (INF '+inflection+')))\n')
				# Single
				cf.write(str(n_freq - n_plural) + '\n')
				ff.write('((SINGULAR w'+str(i)+'))\n')