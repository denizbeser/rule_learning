import numpy as np
import random
import operator
import math

random.seed(1)

defectives = []
with open('defective-verbs.txt', 'r') as inp:
	lines = inp.readlines()
	for i in range(1, len(lines)):
		line = lines[i]
		if i%4==1:
			verb = line.strip().split(',')[0]
			defectives.append(verb)
# print(len(defectives))

#dental verbs to sing
verb2sing = {}
with open('russian-verbs.csv', 'r') as inp:
	lines = inp.readlines()
	for i in range(1,len(lines)):
		tokens = lines[i].split('\t')
		bare = tokens[0]
		accented = tokens[1]
		english = tokens[2]
		fsg = tokens[12]
		
		# get only dental verbs
		if bare[-3:-1] in ['ит','из','ид','ис''иц']:
			j = 0
			while j < len(accented) and j < len(fsg) and accented[j] == fsg[j]: j += 1
			common = fsg[:j]
			inflection = fsg[j:]
			if inflection == '': continue
			# accented form, 1sg, common part, inflection
			verb2sing[bare] = (accented, fsg, common, inflection)

# 20 test verbs
defectives = [v for v in defectives if v in verb2sing.keys()]
print(len(defectives))

# Get random subset, including defective verbs
randomKeys = random.sample(list(verb2sing), 500)
verb2sing = {k:v for k,v in verb2sing.items() if k in randomKeys or k in defectives}

# number of dental verbs
print(len(verb2sing.keys()))

# number of inflections ~30
inflections = set([d[3] for v,d in verb2sing.items()])
print(len(inflections))


#############################
# get to Zipfian distributions
verb2singList = [(v,d) for v,d in verb2sing.items()]
random.shuffle(verb2singList)
zipf = []
length = len(verb2singList)
for rank in range(1,length+1):
	zipf.append(math.floor(length/rank))

# Generate data for FG
count_file = 'russian.counts.txt'
form_file = 'russian.forms.txt'

with open(count_file, 'w') as cf:
	with open(form_file, 'w') as ff:
		# Sort or not (rank random or based on length)
		verb2singList = sorted(verb2singList, key = lambda s: len(s[0]))
		for i in range(len(verb2singList)):
			verb, data = verb2singList[i]
			count = zipf[i]
			inflection = data[3]
				
			# 1SG
			if verb not in defectives:
				cf.write(str(count) + '\n')
				ff.write('((1SG (VERB '+verb+') ' +inflection+'))\n')
				
			# Regular
			cf.write(str(count) + '\n')
			ff.write('((INF (VERB '+verb+')))\n')

# Generate Test Data
with open('test-defectives.txt', 'w') as tf:
	for verb in defectives:
		tf.write(verb+'\n')	
		tf.write(verb+' <.>\n')	
		for inflection in inflections:
			tf.write(verb+' ' +inflection+'\n')

randomKeys = random.sample([v for v,d in verb2singList], 100)
regulars = [k for k in randomKeys if k not in defectives]

with open('test-regulars.txt', 'w') as tf:
	for verb in regulars:
		tf.write(verb+'\n')	
		tf.write(verb+' <.>\n')	
		for inflection in inflections:
			tf.write(verb+' ' +inflection+'\n')

