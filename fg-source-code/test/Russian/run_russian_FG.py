import os
import random
import math
# a = 0.5 and b = 100 are the ones used in the book
# 0 <= a <0
# b > -a

N = 1

a = 0.5
b = 100


rootDir = os.path.abspath('../..')
outDir = rootDir+'/test/Russian/outputs'
data = 'russian'
print('Parameters a=%g, b=%g' % (a,b))

##############################
# Run FG for  each participant
for n in range(N):
	print('n: '+str(n+1))		
	ID = str(n)+'_'+str(a)+'_'+str(b)+'_'+data
	# random_seed = n
	random_seed = random.randint(0,N)
	command = rootDir+'/bin/bestpcfg -corpus-counts '+rootDir+'/test/Russian/'+data+'.counts.txt -corpus-sentences '+rootDir+'/test/Russian/'+data+'.forms.txt -pya '+str(a)+' -pyb '+str(b)+' -sticky-concen 1 -sticky-dist 0.5 -pi 1 -sweeps 1000 -debug-file '+outDir+'/'+ID+'.FG-output-debug-0.txt -output-grammar '+outDir+'/'+ID+'.FG-output-0 -seed '+str(random_seed)+' -start-symbol START'
	os.system(command)

##############################
# Evaluate output
defective_results = {}
regular_results = {}

# For each participant
for n in range(N):
	# Run runpcfg
	ID = str(n)+'_'+str(a)+'_'+str(b)+'_'+data
	defectives_test_file = rootDir+'/test/Russian/test-defectives.txt'
	os.system(rootDir+'/bin/runpcfg -input-grammar '+outDir+'/'+ID+'.FG-output-0.rank-1.txt '
		+'-test-sentences '+defectives_test_file+' -outfile '+outDir+'/'+ID+'.runpcfg-out.txt'
		+'-seeded -debug-file '+outDir+'/'+ID+'.runpcfg-debug.txt'+'-start-symbol START')
	
	# Get the MAP probability
	with open(outDir+'/'+ID+'.runpcfg-out.txt-seeded', 'r') as f:
		lines = f.readlines()
		for i in range(1,len(lines)):
			line = lines[i]
			tokens = line.split(',')
			sentence,mapscore = tokens[0].strip('"'),tokens[4]
			
			words = sentence.split()
			if '<.>' in words:
				defective_results[words[0]] = math.exp(float(mapscore))

	regulars_test_file = rootDir+'/test/Russian/test-regulars.txt'
	os.system(rootDir+'/bin/runpcfg -input-grammar '+outDir+'/'+ID+'.FG-output-0.rank-1.txt '
		+'-test-sentences '+regulars_test_file+' -outfile '+outDir+'/'+ID+'.runpcfg-out.txt'
		+'-seeded -debug-file '+outDir+'/'+ID+'.runpcfg-debug.txt'+'-start-symbol START')
	
		# Get the MAP probability
	with open(outDir+'/'+ID+'.runpcfg-out.txt-seeded', 'r') as f:
		lines = f.readlines()
		for i in range(1,len(lines)):
			line = lines[i]
			tokens = line.split(',')
			sentence,mapscore = tokens[0].strip('"'),tokens[4]
			
			words = sentence.split()
			if '<.>' in words:
				regular_results[words[0]] = math.exp(float(mapscore))

# print(defective_results)
# print(regular_results)
print(sum(defective_results.values())/len(defective_results))
print(sum(regular_results.values())/len(regular_results))



# for key in ['<.>','F','M','N']:
# 	s = results['<WUG> '+key+' s']
# 	n = results['<WUG> '+key+' n']
# 	e = results['<WUG> '+key+' e']
# 	r = results['<WUG> '+key+' r'] if '<WUG> '+key+' r' in results else 0
# 	empty = results['<WUG> '+key+' -']
# 	total = s + n + e + r + empty
# 	print('\n Within '+key)
# 	print('<WUG> '+key+' -', '  p =',empty/total)
# 	print('<WUG> '+key+' e', '  p =',e/total)
# 	print('<WUG> '+key+' r', '  p =',r/total)
# 	print('<WUG> '+key+' n', '  p =',n/total)
# 	print('<WUG> '+key+' s', '  p =',s/total)

# Generate CSV file
# print('Generating CSV...')
# csvfile = 'results_'+experiment+'.csv'
# with open('./'+csvfile, 'w') as f:
# 	f.write(','.join(['Participant','Experiment','Type','Score'])+'\n')
# 	for n in range(N):
# 		for data, types in result_map.items():
# 			for typ, score in types.items():
# 				f.write((str(n)+','+data+','+typ+','+str(score[n])+'\n'))

# Generate R plot
# print('Generating Plot')
# os.system('Rscript plot_results.R'+' '+str(a) + ' ' + str(b) + ' ' + csvfile)