import numpy as np
import random
import operator

Data = np.load('german_data.npy').item() # Format lemma: {S:(wordform, gender, phon, idNum); P:(...)}
genders = {'1':'M', '2':'F','3':'N'} 			# 5. GendNum (1 = M, 2 = F, 3 = N)
count_file = 'german.counts.txt'
form_file = 'german.forms.txt'

# randomKeys = random.sample(list(lemma2Data), 1000)
# lemma2Data = {k:v for k,v in lemma2Data.items() if k in randomKeys}

# Get the most frequent n lemmas
n = 500
sortedLemmas = sorted(Data, key = lambda x: int(Data[x]['f']), reverse = True)[:n]

inflections = set([])
with open (count_file, 'w') as cf:
	with open (form_file, 'w') as ff:
		for lemma in sortedLemmas:
			data = Data[lemma]
			gender = genders[data['P'][1]]
			plural = data['P'][2]
			singular = data['S'][2]
			noun = singular
			# inflection = plural.replace(singular,'')			
			inflection = plural[len(singular):]

			if inflection == '': inflection = '-'
			elif inflection[-1] == '@': inflection = 'e'
			elif inflection[-1] == 'r': inflection = 'r'
			elif inflection[-1] == 's': inflection = 's'
			elif inflection[-1] == 'n': inflection = 'n'
			# skip if inflection changes stem
			# if not (len(inflection) < len(plural)): continue
			inflections.add(inflection)

			# From Schuler Data: ((PLURAL (SINGULAR w0) (INF ka)))  or ((SINGULAR w0))
			# ff.write('((PLURAL (SINGULAR w'+str(i)+') (INF '+inflection+')))\n')

			# Plural: ((PLURAL (SINGULAR (NOUN w) (GENDER g)) (INF inf)))
			cf.write(str(1) + '\n')
			ff.write('((PLURAL (NOUN '+noun+') '+gender+' '+inflection+'))\n')
			
			# Singular (SINGULAR (NOUN w) (GENDER g))
			cf.write(str(1) + '\n')
			ff.write('((SINGULAR (NOUN '+noun+') '+gender+'))\n')
			# print out lines for FG training

print(inflections)