##########################################################################################	
##
## EVALUATION
##

import os
import sys
import numpy as np

outDir = './outputs/'
# data_lists = ['3r6e','5r4e']#'4r5e'
data_lists = ['3r6e-x2-LangA','5r4e-x2-LangA','3r6e-x2-LangB','5r4e-x2-LangB']

args = sys.argv[1:]
a = float(args[0])
b = int(float(args[1]))
N = int(float(args[2]))

## Evaluate outputs from the FG model.

print('Evaluating Exp. 2 outputs...')

result_map = {data:{'R':[],'e':[],'null':[]} for data in data_lists}
for n in range(N):
	for data in data_lists:
		ID = str(n)+'_'+str(a)+'_'+str(b)+'_'+data
		with open(outDir+'/'+ID+'.FG-output-0.rank-1.txt', 'r') as f:
			# Check if categorical learning has happened; learned only 'ka' as the productive rule
			learned_R = 0
			max_R_score = -float('inf')
			learned_e = 0
			max_e_score = -float('inf')
			rules = []
			for line in f:
				if 'PLURAL  SINGULAR _ka' in line: 
					score = float(line.split()[0])
					if score > max_R_score: max_R_score = score
					learned_R = 1
				if 'PLURAL  SINGULAR _e_' in line: 
					score = float(line.split()[0])
					if score > max_e_score: max_e_score = score
					learned_e = 1

			# combine lang a participants; combine lang bs participants 
			key = data
			result_map[key]['R'].append(0)
			result_map[key]['e'].append(0)
			result_map[key]['null'].append(0)
			if not learned_R and not learned_e: result_map[key]['null'][-1] = 1
			elif max_R_score > max_e_score: result_map[key]['R'][-1] = 1
			else: result_map[key]['e'][-1] = 1

# Report parameters and score
# with open('./report.txt', 'a') as f:
# 	f.write('a='+str(a)+';b='+str(b)+';')
# 	results = {data: np.mean(values) for data, values in result_map.items()}
# 	for data, mean in results.items():
# 		f.write(data + '_mean:' + str(mean) + ';')
# 	f.write('dif:' + str(results['5r4e'] - results['3r6e']) + '\n')

# Generate CSV file
print('Generating CSV...')
csvfile = 'results_exp2.csv'
with open('./'+csvfile, 'w') as f:
	f.write(','.join(['Participant','Experiment','Type','Score'])+'\n')
	for n in range(N):
		for data, types in result_map.items():
			for typ, score in types.items():
				f.write((str(n)+','+data+','+typ+','+str(score[n])+'\n'))

# Generate R plot
print('Generating Plot')
os.system('Rscript plot_results.R'+' '+str(a) + ' ' + str(b) + ' ' + csvfile)


