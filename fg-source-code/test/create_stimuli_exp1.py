# Create Tolerance Principle test datasets, in FG format

rootDir = '/Users/denizbeser/Desktop/Stuff/Penn/Junior_Summer/Rule_Learning/fg-source-code'
# data_lists = ['3r6e','4r5e','5r4e']
data_lists = ['3r6e-x1','5r4e-x1']

freq_count_list = [24,12,8,6,6,4,4,4,4]
plural_count_list = [16,8,5,4,4,3,3,3,3]

for data in data_lists:
	count_file = rootDir+'/data/'+data+'.counts.txt'
	form_file = rootDir+'/data/'+data+'.forms.txt'
	with open (count_file, 'w') as cf:
		with open (form_file, 'w') as ff:
			n_reg = int(data[0])
			n_exp = int(data[2])
			# For each noun, print n_p plurals and remaining as singles
			for i in range(9):
				n_freq = freq_count_list[i]
				n_plural = plural_count_list[i]
				# Pick inflection
				inflection = 'ka' if i < n_reg else 'e_'+str(i)
				# Plural
				cf.write(str(n_plural) + '\n')
				ff.write('((PLURAL (SINGULAR w'+str(i)+') (INF '+inflection+')))\n')
				# Single
				cf.write(str(n_freq - n_plural) + '\n')
				ff.write('((SINGULAR w'+str(i)+'))\n')