require 'fileutils'

#Files:
#  <simulation> should be of the form <identifier>-<identifier>*-<identifier>
#
#  Input/<simulation>.forms.txt                      :  Input forms files (user specified)
#  Input/<simulation>.counts.txt                     :  Input counts file (user specified)
#  Input/<simulation>.extra-tests.txt                :  Additional, non-generated test cases (user specified)
#  Stages/<simulation>.tests.txt                     :  Generate test cases (includes the ones above)
#  Stages/<simulation>.FG-output-debug-<num>.txt     :  Debug file for FG run number <num>
#  Stages/<simulation>.FG-output-<num>.txt           :  Output PCFG for FG run number <num>
#  Stages/<simulation>.<type>-gram.txt               :  Grammar file for each type (note that the FG one is an average)
#  Stages/<simulation>.posterior-estimate.txt        :  Posterior Estimate File
#  Stages/<simulation>.<type>-results.txt            :  Test  results file for each type
#  Stages/<simulation>.<type>-results-debug.txt      :  Test results debug file for each type
#  Results/<simulation>.merged-results.txt            :  Merged results for all test cases

MAXIMUM_FG_RUNS = 50

PYA = 0.5
PYB = 100
STICK_CONCEN = 1
STICK_DIST = 0.5
PI = 1
SWEEPS = 1000
START_SYMBOL = 'START'

SEEDED = "" #"-seeded" #make empty for non-seeded run of runpcfg tests

STAGES_DIR = "Stages"
INPUT_DIR = "Inputs"
RESULTS_DIR = "Results"

BEST_PCFG = "#{ENV['FGPREFIX']}/bin/bestpcfg"
RUN_PCFG = "#{ENV['FGPREFIX']}/bin/runpcfg"
ALTERNATES_SCRIPT =  "#{ENV['FGPREFIX']}/Tools/GenerateAlternateGrammars/generate-alternate-grammars.ss"

#if ENV['FGPREFIX'] == "/Users/timo/Projects/fragment-grammar"
  LOG0 = Math.log(0.0)
#else
#  LOG0 = ""
#end

LOG1 = Math.log(1.0)

#user specified
INPUT_FORMS_INFIX = "forms"
INPUT_COUNTS_INFIX = "counts"
INPUT_TESTS_INFIX = "extra-tests"

#constructed automatically
TEST_INFIX = "tests"
FG_DEBUG_INFIX = "FG-output-debug"
FG_OUTPUT_INFIX = "FG-output"
MDPCFG_GRAM_INFIX  = "MDPCFG-gram"
GRAM_INFIX  = "gram"
POSTERIOR_ESTIMATE_INFIX = "posterior-estimate"
RESULTS_INFIX = "results"
RESULTS_DEBUG_INFIX = "results-debug"
MERGED_RESULTS_INFIX = "merged-results"

#different simulation we know how to run
SIMULATION_TYPES = ["MDPCFG","AG","DOP1","FG", "GDMN"]
ALTERNATES = ["MDPCFG","AG","DOP1","GDMN"]
#ALTERNATES = ["MDPCFG"]

def logsumexp( values )
  #$stderr.puts "#{values}\n" # Thang
  Math.log((values.map {|v| v - values.max}).inject(0) {|acc,val| acc + Math.exp(val)}) + values.max
end

def find_inputs( input_directory )
  $stderr.puts "Finding simulations in #{input_directory}/Inputs"
  
  simulations = Hash.new {|hash,key| hash[key] = {:Name => [], :Numbers=>[]}}

  Dir.glob("#{input_directory}/Inputs/*.forms.txt") do |path|
    $stderr.puts "#{path}\n'" # Thang
    if /^([^.]+)\.num-(\d+)\.([^-]+).txt$/ =~ path then
      num = $2.to_i
      simulation = $1.gsub(/.*?\/([^\/]+)$/,"\\1").split('-')
      $stderr.puts "Found New simulation: #{simulation.inspect} number: #{num}"
      simulations[simulation][:Name] = simulation
      simulations[simulation][:Numbers].push(num)
    elsif /^([^.]+)\.([^-]+).txt$/ =~ path
      simulation = $1.gsub(/.*?\/([^\/]+)$/,"\\1").split('-')
      $stderr.puts "Found New simulation: #{simulation.inspect}"
      simulations[[simulation,num]][:Name] = simulation 
    end
  end
  
  simulations
end
