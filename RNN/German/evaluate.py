with open('data/pred.txt','r') as pred:
	predlines = pred.readlines()
with open('data/german-tgt-test.txt','r') as tgt:	
	tgtlines = tgt.readlines()
with open('data/german-src-test.txt','r') as src:	
	srclines = src.readlines()

tups = list(zip(srclines,tgtlines,predlines))

m = []
f = []
n = []
for src,tgt,pred in tups:
	# get the learned inflection
	src,tgt,pred = src.strip(),tgt.strip(),pred.strip()
	gender = src[1]
	# print(gender)
	gold = '' 
	if tgt[-1] == '@': gold = 'e'
	elif tgt[-1] == 'r': gold = 'r'
	elif tgt[-1] == 's': gold = 's'
	elif tgt[-1] == 'n': gold = 'n'

	learned = ''
	if pred[-1] == '@': learned = 'e'
	elif pred[-1] == 'r': learned = 'r'
	elif pred[-1] == 's': learned = 's'
	elif pred[-1] == 'n': learned = 'n'
	
	if gender == 'M': m.append((gold,learned))
	elif gender == 'F': f.append((gold,learned))
	elif gender == 'N': n.append((gold,learned))

print('Test accuracy:')
print('M:',sum([t[0]==t[1]for t in m])/len(m))
print('F:',sum([t[0]==t[1]for t in f])/len(f))
print('N:',sum([t[0]==t[1]for t in n])/len(n))

##### Wug Test
with open('data/german-wug.txt','r') as f:
	wuglines = f.readlines()
with open('data/pred-wug.txt','r') as f:	
	wugpredlines = f.readlines()

wugtups = list(zip(wuglines,wugpredlines))
inflections = [t[1][-2] if len(t[1]) > 1 else '-' for t in wugtups]
# print(inflections)
print('\nWUG Tests:')
print('-s',sum([t == 's' for t in inflections])/len(inflections))
print('-e',sum([t == '@' for t in inflections])/len(inflections))
print('-r',sum([t == 'r' for t in inflections])/len(inflections))
print('-n',sum([t == 'n' for t in inflections])/len(inflections))
print('Other:',sum([t not in ['n','@','r','s'] for t in inflections])/len(inflections))