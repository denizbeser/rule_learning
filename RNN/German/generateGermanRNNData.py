import numpy as np
import random
import operator

random.seed(1)

Data = np.load('german_data.npy').item() # Format lemma: {S:(wordform, gender, phon, idNum); P:(...)}
genders = {'1':'M', '2':'F','3':'N'} 			# 5. GendNum (1 = M, 2 = F, 3 = N)

# print(len(Data)) 20k words
# Get the most frequent n lemmas
n = 5500
sortedLemmas = sorted(Data, key = lambda x: int(Data[x]['f']), reverse = True)[:n]

verbs = []

for lemma in sortedLemmas:
	data = Data[lemma]
	gender = genders[data['P'][1]]
	plural = data['P'][2]
	singular = data['S'][2]

	verbs.append((singular,'<'+gender+'>',plural))

random.shuffle(verbs)


with open('data/german-src-train.txt','w') as src:
	with open('data/german-tgt-train.txt','w') as tgt:
		for s,g,p in verbs[0:4000]:
			src.write(g+' '+' '.join(list(s))+'\n')
			tgt.write(' '.join(list(p))+'\n')
with open('data/german-src-val.txt','w') as src:
	with open('data/german-tgt-val.txt','w') as tgt:
		for s,g,p in verbs[4000:4500]:
			src.write(g+' '+' '.join(list(s))+'\n')
			tgt.write(' '.join(list(p))+'\n')
with open('data/german-src-test.txt','w') as src:
	with open('data/german-tgt-test.txt','w') as tgt:
		for s,g,p in verbs[4500:5000]:
			src.write(g+' '+' '.join(list(s))+'\n')
			tgt.write(' '.join(list(p))+'\n')

with open('data/german-wug.txt','w') as wug:
	for s,g,p in verbs[5000:5500]:
		wug.write(' '.join(list(s))+'\n')