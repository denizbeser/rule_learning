with open('data/pred.txt','r') as pred:
	predlines = pred.readlines()
with open('data/russian-tgt-test.txt','r') as tgt:	
	tgtlines = tgt.readlines()
with open('data/russian-src-test.txt','r') as src:	
	srclines = src.readlines()

tups = list(zip(srclines,tgtlines,predlines))

r = []
for src,tgt,pred in tups:
	# get the learned inflection
	src,tgt,pred = src.strip(),tgt.strip(),pred.strip()
	gold = tgt
	learned = pred
	# if tgt[-1] == '@': gold = 'e'
	# elif tgt[-1] == 'r': gold = 'r'
	# elif tgt[-1] == 's': gold = 's'
	# elif tgt[-1] == 'n': gold = 'n'

	# learned = ''
	# if pred[-1] == '@': learned = 'e'
	# elif pred[-1] == 'r': learned = 'r'
	# elif pred[-1] == 's': learned = 's'
	# elif pred[-1] == 'n': learned = 'n'
	
	r.append((gold,learned))

print('Test accuracy:',sum([t[0]==t[1]for t in r])/len(r))

##### Gap Test
with open('data/russian-defectives.txt','r') as f:
	deflines = f.readlines()
with open('data/pred-defectives.txt','r') as f:	
	defpredlines = f.readlines()

deftups = list(zip(deflines,defpredlines))
for t in deftups:
	bare,pred = ''.join(t[0].split()),''.join(t[1].split())
	if bare[0] == pred[0]:
		print(bare, '-', pred)
# print(deftups)
# inflections = [t[1][-2] if len(t[1]) > 1 else '-' for t in wugtups]
# print(inflections)
# print('\nWUG Tests:')
# print('-s',sum([t == 's' for t in inflections])/len(inflections))
# print('-e',sum([t == '@' for t in inflections])/len(inflections))
# print('-r',sum([t == 'r' for t in inflections])/len(inflections))
# print('-n',sum([t == 'n' for t in inflections])/len(inflections))
# print('Other:',sum([t not in ['n','@','r','s'] for t in inflections])/len(inflections))