import os 

cwd = os.getcwd()
nmtdir = '../OpenNMT-py-master/'

epochs, n_examples, batchsize,  = 300, 2000, 20
steps = str(int(epochs * n_examples / batchsize))

# For each experiment, first run training, and then translation
expdir = cwd+'/data'

modelpath = expdir+'/'+'model'
train_args = [
	'-data '+expdir+'/processed',
	'-save_model '+modelpath,
	'-enc_layers 2',
	'-dec_layers 2',
	'-rnn_size 100',
	'-batch_size 20',
	'-word_vec_size 300',
	# '-gpu_ranks 0',
	'-train_steps '+steps,
	'-save_checkpoint_steps '+steps
	]

trans_args = [
	 '-model '+modelpath+'_step_'+steps+'.pt',
	 '-src '+expdir+'/russian-src-test.txt',
	 '-output '+expdir+'/'+'pred.txt',
	 '-replace_unk -verbose',
	 '-beam_size 12'
	]

trans_args_def = [
	 '-model '+modelpath+'_step_'+steps+'.pt',
	 '-src '+expdir+'/russian-defectives.txt',
	 '-output '+expdir+'/'+'pred-defectives.txt',
	 '-replace_unk -verbose',
	 '-beam_size 12'
	]

os.system('python3 '+nmtdir+'preprocess.py -train_src '+expdir+'/russian-src-train.txt -train_tgt '+expdir+'/russian-tgt-train.txt -valid_src '+expdir+'/russian-src-val.txt -valid_tgt '+expdir+'/russian-tgt-val.txt -save_data '+expdir+'/processed')
os.system('python3 '+nmtdir+'train.py '+' '.join(train_args))
os.system('python3 '+nmtdir+'translate.py '+' '.join(trans_args))
os.system('python3 '+nmtdir+'translate.py '+' '.join(trans_args_def))