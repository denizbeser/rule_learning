import os

### Create Data set for testing OpenNMT RNN on Schuler Data 

rootDir = '/Users/denizbeser/Desktop/Stuff/Penn/Junior_Summer/Rule_Learning/RNN/tests'

# types of training data
exp_lists = ['3r6e-x1','5r4e-x1',
	'3r6e-x2-LangA','5r4e-x2-LangA','3r6e-x2-LangB','5r4e-x2-LangB']

#freq_count_list = [24,12,8,6,6,4,4,4,4] # total frequency
plural_count_list = [16,8,5,4,4,3,3,3,3]
exp2index = {'3r6e-x2-LangA':[1,0,1,0,1,0,0,0,0],'5r4e-x2-LangA':[1,0,1,0,1,0,1,0,1],
	'3r6e-x2-LangB':[0,1,0,1,0,1,0,0,0],'5r4e-x2-LangB':[0,1,0,1,0,1,0,1,1]}

n_train_types = len(plural_count_list)
n_val_types = 100
n_test_types = 100

# Generate data files for each experiment
for exp in exp_lists:
	# Setup directory for experiment
	exp_dir = rootDir+'/data/'+exp+'/'
	if os.path.exists(exp_dir): os.system('rm -r '+exp_dir)
	os.makedirs(exp_dir)
	src_train_path = exp_dir+'src-train.txt'
	tgt_train_path = exp_dir+'tgt-train.txt'
	src_val_path = exp_dir+'src-val.txt'
	tgt_val_path = exp_dir+'tgt-val.txt'
	src_test_path = exp_dir+'src-test.txt'
	# Generate training files
	with open (src_train_path, 'w') as srctrainf:
		with open (tgt_train_path, 'w') as tgttrainf:
			# for each type in training set
			for i in range(n_train_types):
				n_tokens = plural_count_list[i]
				# Pick inflection
				# X1: is i < N regular? X2: is i an index where data should be regular?
				isRegular = exp2index[exp][i] if 'x2' in exp else (i < int(exp[0]))
				inflection = 'ka' if isRegular else 'e_'+str(i)
				# n_tokens is the number of tokens
				for j in range(n_tokens):
					srctrainf.write(' '.join(list('train_w_'+str(i)+'\n')))
					tgttrainf.write(' '.join(list('train_w_'+str(i)+inflection+'\n')))

	# Generate validation files
	with open (src_val_path, 'w') as srcvalf:
		with open (tgt_val_path, 'w') as tgtvalf:
			# Inflection is always 'ka'
			inflection = 'ka'
			for i in range(n_val_types):
				srcvalf.write(' '.join(list('val_w_'+str(i)+'\n')))
				tgtvalf.write(' '.join(list('val_w_'+str(i)+inflection+'\n')))

	# Generate test files
	with open (src_test_path, 'w') as srctestf:
		for i in range(n_test_types):
			srctestf.write(' '.join(list('test_w_'+str(i)+'\n')))
	
# Preprocess data

cwd = os.getcwd()
nmtdir = '../OpenNMT-py/'
for experiment in os.listdir(cwd+'/data/'):
	if '.DS' in experiment: continue
	expdir = cwd+'/data/'+experiment
	os.system('python '+nmtdir+'preprocess.py -train_src '+expdir+'/src-train.txt -train_tgt '+expdir+'/tgt-train.txt -valid_src '+expdir+'/src-val.txt -valid_tgt '+expdir+'/tgt-val.txt -save_data '+expdir+'/processed')
