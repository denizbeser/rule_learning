import os 

cwd = os.getcwd()
nmtdir = '../OpenNMT-py/'

epochs, n_examples, batchsize,  = 100, 50, 20
steps = str(int(epochs * n_examples / batchsize))

# For each experiment, first run training, and then translation
# data = '/artificial_data/'
data = '/past_tense_data/'

for experiment in os.listdir(cwd+data):
	if '.DS' in experiment: continue
	expdir = cwd+data+experiment
	modelpath = expdir+'/'+experiment+'-model'
	train_args = [
		'-data '+expdir+'/processed',
		'-save_model '+modelpath,
		'-enc_layers 2',
		'-dec_layers 2',
		'-rnn_size 100',
		'-batch_size 20',
		'-word_vec_size 300',
		'-train_steps '+steps,
		# '-gpuid 0',
		'-save_checkpoint_steps '+steps
		]
	os.system('python3 '+nmtdir+'train.py '+' '.join(train_args))

	trans_args = [
		 '-model '+modelpath+'_step_'+steps+'.pt',
		 '-src '+expdir+'/src-test.txt',
		 '-output '+cwd+'/'+experiment+'-pred.txt',
		 '-replace_unk -verbose',
		 '-beam_size 12'
		]
	os.system('python3 '+nmtdir+'translate.py '+' '.join(trans_args))
